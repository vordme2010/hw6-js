// цикл .forEach() выполняет функцию callback для каждого элемента массива, но в отличии от .map() ничего не создаёт
// потому считается плохим тоном делать из метода .map() - forEach()

// -----------способ с помошью .filter()-----------
const arrayOfData = [    
    'hello', 'world', 23, '23', null, true
]
// function filterByFilter(array, dataType){
//     const filteredArr = array.filter(value => {
//         if (value === null && dataType === "object") {
//             return typeof value === dataType
//         }
//         return typeof value !== dataType
//     })
//     return filteredArr
// }
// console.log(filterByFilter(arrayOfData, "object"))

// -----------способ с помошью .forEach()-----------
function filterByForEach(array, dataType){
    const filtered = []
        array.forEach(function(element) {
            if (dataType === "object" && element === null) {
                if (typeof element === dataType) {
                    filtered.push(element)
                }
            }
            if (typeof element !== dataType) {
                filtered.push(element)
            }
        })
    console.log(filtered)
}
filterByForEach(arrayOfData, "object")

// -----------проверка быстродействия двух способов ака бенчмарк-----------
// function benchmark(func) {
//     let date = new Date();
//     for (let i = 0; i < 10000; i++) func(arrayOfData);
//     return new Date() - date;
//   }
// alert( 'Время с методом .forEach() : ' + benchmark(filterByForEach) + 'мс' );
// alert( 'Время с методом .filter() : ' + benchmark(filterByFilter) + 'мс' );

